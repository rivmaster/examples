package tv.imaqliq.send;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    public static final String RECEIVER_ACTION_BOOKMARKS = "tv.imaqliq.action.Bookmarks";
    private String TAG = "MainActivityTag";

    public String broadcast_titles[] = {"HTB","ТНТ"};
    public String broadcast_contents[] = {"Description", "Description"};
    public String broadcast_urls[] = {"https://img2.ntv.ru/home/news/20120622/ntv2.jpg","https://cdn.tnt-online.ru/tnt2012/tnt226x226.png"};
    public String broadcast_videoUrls[] = {"",""};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void sendMessage(View view){

        Log.d(TAG,"sendMessage call");

        Intent intent = new Intent();
//        intent.setClassName("com.jacky.launcher", "com.jacky.launcher.MyBroadcastReceiver");
        intent.setAction(RECEIVER_ACTION_BOOKMARKS);

        intent.putExtra("tv.imaqliq.action.bookmarks.Titles",broadcast_titles);
        intent.putExtra("tv.imaqliq.action.bookmarks.Descriptions",broadcast_contents);
        intent.putExtra("tv.imaqliq.action.bookmarks.Icons",broadcast_urls);
        intent.putExtra("tv.imaqliq.action.bookmarks.Urls",broadcast_videoUrls);




        sendBroadcast(intent);
    }
}
